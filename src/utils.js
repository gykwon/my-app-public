export function isAllTrue(a, b) {
  if (a) {
    if (b) {
      return true;
    }
  }

  return false;
}

export function isAllFalse(a, b) {
  if (!a) {
    if (!b) {
      return true;
    }
  }

  return false;
}
