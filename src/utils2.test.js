import { add, sub } from './utils2.js';

it('add', () => {
  var result = add(1, 2);
  expect(result).toBe(3);
});

it('sub', () => {
  var result = sub(2, 1);
  expect(result).toBe(1);
});
