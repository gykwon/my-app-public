import { isAllTrue, isAllFalse } from './utils.js';

it('all true', () => {
  var result = isAllTrue(true, true);
  expect(result).toBe(true);
});

it('all true', () => {
  var result = isAllTrue(true, false);
  expect(result).toBe(false);
});

it('all true', () => {
  var result = isAllTrue(false, false);
  expect(result).toBe(false);
});

it('all true', () => {
  var result = isAllTrue(false, true);
  expect(result).toBe(false);
});

it('all false', () => {
  var result = isAllFalse(false, false);
  expect(result).toBe(true);
});

it('all false', () => {
  var result = isAllFalse(true, false);
  expect(result).toBe(false);
});

it('all false', () => {
  var result = isAllFalse(true, true);
  expect(result).toBe(false);
});

it('all false', () => {
  var result = isAllFalse(false, true);
  expect(result).toBe(false);
});
